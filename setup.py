#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "googleapi",
    version = "0.1.2",
    url = 'http://ondrejsika.com/docs/python-googleapi',
    download_url = 'https://github.com/sikaondrej/python-googleapi',
    license = 'GNU LGPL v.3',
    description = "",
    author = 'Ondrej Sika',
    author_email = 'dev@ondrejsika.com',
    packages = find_packages(),
    requires = ["re", "json", "urllib"],
    include_package_data = True,
    zip_safe = False,
)

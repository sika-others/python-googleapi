python-googleapi
================

Library for smart usage of Google API

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, dev@ondrejsika.com

### Source
* Python Package Index: <http://pypi.python.org/pypi/googleapi>
* GitHub: <https://github.com/sikaondrej/python-googleapi>


Documentation
-------------

### Instalation
Instalation is very simple over pip.

    pip install googleapi

### Search
#### Web search

`googleapi.search.web(query, [results_count=8])`
    
    >>> from googleapi import search
    >>> search.web(query="Ondrej Sika", results_count=2)
    [
        {
            'url': u'http://ondrejsika.com/',
            'description': u'Checkout some of my sites: ProdejKnihu.cz (CZ), OndrejSika's blog (CZ), BitBucket, GitHub, ...',
            'title': u'OndrejSika',
        },
        {
            'url': u'https://github.com/sikaondrej',
            'description': u'sikaondrej has 8 repos written in Python, JavaScript, and C.',
            'title': u'sikaondrej(Ond\u0159ej\u0160ika)\xb7GitHub'
        }
    ]

#### Images search

`googleapi.search.images(query, [results_count=8])`

    >>> from googleapi import search
    >>> search.images(query="Ondrej Sika", results_count=2)
    [
        {
            'description_url': u'http://www.facebook.com/sikaondrej2',
            'image_url': u'http://sphotos-a.xx.fbcdn.net/hphot461_o.jpg',
            'description': u'Ondrej Sika',
            'title': u'OndrejSika | Facebook'
        },
        {
            'description_url': u'http://www.facebook.com/sikaondrej2',
            'image_url': u'http://profile.ak.fbcdn.net/hp0_n.jpg',
            'description': u'Ondrej Sika',
            'title': u'390499_343806985714320_...'
        }
    ]
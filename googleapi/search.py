# googleapi
# authors: Ondrej Sika, http://ondrejsika.com, dev@ondrejsika.com

from utils import get_json, make_url, strip_tags

from conf import MAX_RESULTS_COUNT

def search_base(query, search_type="web", results_count=MAX_RESULTS_COUNT):
    if results_count > MAX_RESULTS_COUNT:
        results_count = MAX_RESULTS_COUNT
    url = 'http://ajax.googleapis.com/ajax/services/search/%s' % search_type
    url = make_url(url, {
        "v": "1.0",
        "q": query,
        "rsz": results_count,
    })
    return get_json(url)

def web(query, results_count=MAX_RESULTS_COUNT):
    results_base = search_base(query, "web", results_count)
    results = []
    for result in results_base["responseData"]["results"]:
        results.append({
            "url": result["url"],
            "title": result["titleNoFormatting"],
            "description": strip_tags(result["content"]),
        })
    return results

def images(query, results_count=MAX_RESULTS_COUNT):
    results_base = search_base(query, "images", results_count)
    results = []
    for result in results_base["responseData"]["results"]:
        results.append({
            "image_url": result["url"],
            "thumbnail_url": result["tbUrl"],
            "title": result["titleNoFormatting"],
            "description": result["contentNoFormatting"],
            "description_url": result["originalContextUrl"],
        })
    return results

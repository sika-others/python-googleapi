# googleapi
# authors: Ondrej Sika, http://ondrejsika.com, dev@ondrejsika.com

import json

from utils import get_json, make_url, strip_tags, get

class Video:
    def __init__(self, video_id):
        self.video_id = video_id

    def get_raw_info(self):
        try:
            return get_json("https://gdata.youtube.com/feeds/api/videos/%s?v=2&alt=json"%self.video_id)
        except:
            return {}
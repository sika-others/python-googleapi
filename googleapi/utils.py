# googleapi
# authors: Ondrej Sika, http://ondrejsika.com, dev@ondrejsika.com

import json
import urllib
import re

def get(url):
    response = urllib.urlopen(url)
    return response.read()

def get_json(url):
    return json.loads(get(url))

def make_url(url, data_dict):
    return "%s?%s" % (url, urllib.urlencode(data_dict))

def strip_tags(value):
    return re.sub(r'<[^>]*?>', '', value)